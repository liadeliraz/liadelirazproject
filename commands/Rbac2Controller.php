<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class Rbac2Controller extends Controller
{

	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexItem = $auth->createPermission('indexItem');
		$indexItem->description = 'Admin can view all items';
		$auth->add($indexItem);
		
		$deleteItem = $auth->createPermission('deleteItem');
		$deleteItem->description = 'Admin can delete  item';
		$auth->add($deleteItem);
		
		$viewItem = $auth->createPermission('viewItem');
		$viewItem->description = 'Admin can view each item';
		$auth->add($viewItem);
		
		$createItem = $auth->createPermission('createItem');
		$createItem->description = 'Admin can create new item';
		$auth->add($createItem);
		
		$updateItem = $auth->createPermission('updateItem');
		$updateItem->description = 'Admin can update  employee';
		$auth->add($updateItem);
	}
		
		
	public function actionChilds()
	{
		$auth = Yii::$app->authManager;
		$admin = $auth->getRole('admin');
		
		$deleteItem = $auth->getPermission('deleteItem');
		$auth->addChild($admin, $deleteItem);
		
		$viewItem = $auth->getPermission('viewItem');
		$auth->addChild($admin, $viewItem);
		
		$indexItem = $auth->getPermission('indexItem');
		$auth->addChild($admin, $indexItem);
		
		$createItem = $auth->getPermission('createItem');
		$auth->addChild($admin, $createItem);
		
		$updateItem = $auth->getPermission('updateItem');
		$auth->addChild($admin, $updateItem);
		
		
	}
		
		
		
		
}