<?php

namespace app\controllers;

use Yii;
use app\models\Employees;
use app\models\EmployeesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Role;
use app\models\Armed;
use app\models\PercentOfJobs;
use yii\web\UnauthorizedHttpException;
use yii\filters\AccessControle;
use yii\web\UploadedFile;
use app\models\UploadForm;


/**
 * EmployeesController implements the CRUD actions for Employees model.
 */
class EmployeesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
         return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    /**
     * Lists all Employees models.
     * @return mixed
     */
    public function actionIndex()
    {
        
		if (!\Yii::$app->user->can('indexEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
		$searchModel = new EmployeesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'roles' => Role::getRolesWithAllRoles(),
			'role' => $searchModel->role,
			'armeds' => Armed::getArmedsWithAllArmeds(),
			'armed' => $searchModel->armed,
			'PercentOfJobss' => PercentOfJobs::getPercentOfJobssWithAllPercentOfJobss(),
			'Percent_of_jobs' => $searchModel->Percent_of_jobs,
		
        ]);
    }

    /**
     * Displays a single Employees model.
     * @param integer $id
     * @return mixed
     */
	 
	 
	 public function actionExport()
    {
		$employeeExport= Employees::find()->all();
		\moonland\phpexcel\Excel::widget([
			'models' => $employeeExport,
			'mode' => 'export', //default value as 'export'
			'columns' => ['id','first_name','last_name','cellphone','adress','role','armed','Percent_of_jobs','email'], //without header working, because the header will be get label from attribute label. 
			'headers' => ['id' => 'ת.ז', 'first_name' => 'שם פרטי','last_name' => 'שם משפחה', 'cellphone' => 'פלאפון', 'adress' => 'כתובת', 'role' => 'תפקיד', 'armed' => 'סוג נשק','Percent_of_jobs' =>'אחוז משרה','email'=>'מייל'], 
			'fileName' => 'Employees',
	
		]);
	}
	 
    public function actionView($id)
    {
		if (!\Yii::$app->user->can('viewEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employees model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       
		if (!\Yii::$app->user->can('createEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
		$model = new Employees();

        if ($model->load(Yii::$app->request->post())) 
		{
			
			//מקבל מופע של העלאת הקובץ
			$imageName = $model->first_name;
			$model->file = UploadedFile::getInstance($model,'file');
			$model->file->saveAs('uploads/'.$imageName.'.'.$model->file->extension);
			
			//שומר את הנתיב בדאטא בייס
			$model->image ='uploads/'.$imageName.'.'.$model->file->extension; 
			
			$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Employees model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	
	
        $model = $this->findModel($id);
		if (!\Yii::$app->user->can('updateEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
        if ($model->load(Yii::$app->request->post()) ) 
		{
			//מקבל מופע של העלאת הקובץ
			$imageName = $model->first_name;
			$model->file = UploadedFile::getInstance($model,'file');
			$model->file->saveAs('uploads/'.$imageName.'.'.$model->file->extension);
			
			//שומר את הנתיב בדאטא בייס
			$model->image ='uploads/'.$imageName.'.'.$model->file->extension; 
			
			
			$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Employees model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	
		if (!\Yii::$app->user->can('deleteEmployee'))
			throw new UnauthorizedHttpException ('שלום, אתה לא מורשה לבצע פעולה זו!');
	
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Employees model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employees the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employees::findOne($id)) !== null) {
           
			return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 
	
	
	
	
	
	
	
}
