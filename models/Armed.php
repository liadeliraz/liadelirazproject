<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "armed".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Employees $employees
 */
class Armed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'armed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	
	public static function getArmeds()
	{
		$allArmeds = self::find()->all();
		$allArmedsArray = ArrayHelper::
					map($allArmeds, 'id', 'name');
		return $allArmedsArray;						
	}
	
	public static function getArmedsWithAllArmeds()
	{
		$allArmeds = self::getArmeds();
		$allArmeds[-1] = 'כל המצבים';
		$allArmeds = array_reverse ( $allArmeds, true );
		return $allArmeds;	
	}		
	
	
	

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeess()
    {
        return $this->hasMany(Employees::className(), ['armed' => 'id']);
    }
}
