<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bstatus".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Buroc[] $burocs
 */
class Bstatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bstatus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

	
	
	
	
	public static function getBstatuss()
	{
		$allBstatuss = self::find()->all();
		$allBstatussArray = ArrayHelper::
					map($allBstatuss, 'id', 'name');
		return $allBstatussArray;						
	}
	
	public static function getBstatussWithAllBstatuss()
	{
		$allBstatuss = self::getBstatuss();
		$allBstatuss[-1] = 'הכל';
		$allBstatuss = array_reverse ( $allBstatuss, true );
		return $allBstatuss;	
	}		
	
	
	
	
	
	
	
	
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBurocs()
    {
        return $this->hasMany(Buroc::className(), ['bstatus' => 'id']);
    }
}
