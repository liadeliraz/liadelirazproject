<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;	
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use yii\base\Model;
/**
 * This is the model class for table "employees".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property integer $role
 * @property integer $Percent_of_jobs
 * @property integer $cellphone
 * @property string $adress
 * @property string $armed
 *
 * @property Role $role0
 */
class Employees extends \yii\db\ActiveRecord
{
    
	public $file;

	
	public static function tableName()
    {
        return 'employees';
    }
/**
* access url
**/
	
	
	  
	
	
 
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'first_name', 'last_name', 'role', 'Percent_of_jobs', 'cellphone', 'adress', 'armed','file'], 'required','message'=>'שדה חובה'],
            [['id', 'role', 'Percent_of_jobs', 'created_at','updated_at','created_by','updated_by'], 'integer','message'=>'ספרות בלבד'],
			['email', 'email'],
			[['file'],'file','skipOnEmpty' => true],
            [['first_name', 'last_name', 'adress', 'armed','image','cellphone'], 'string', 'max' => 255],
            
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ת.ז',
            'first_name' => 'שם פרטי',
            'last_name' => 'שם משפחה',
            'role' => 'תפקיד',
            'Percent_of_jobs' => 'אחוזי משרה',
            'cellphone' => 'טלפון',
            'adress' => 'כתובת',
			'email'  => 'מייל',
            'armed' => 'נשק',
			'file' => 'image',
			'created_at' => 'created_at',
			'updated_at' => 'updated_at',
			'created_by' => 'created_by',
			'updated_by' => 'updated_by',
        ];
    }
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	public function getFullname()
    {
        return $this->first_name.' '.$this->last_name;
    }
	
	public static function getEmployees()
	{
		$allEmployees = self::find()->all();
		$allEmployeesArray = ArrayHelper::
					map($allEmployees, 'id', 'fullname');
		return $allEmployeesArray;						
	}
	
	
	
	
	public function getRoleItem()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }
	
	public function getArmedItem()
    {
        return $this->hasOne(Armed::className(), ['id' => 'armed']);
    }
	
	public function getPercent_of_jobsItem()
    {
        return $this->hasOne(PercentOfJobs::className(), ['id' => 'Percent_of_jobs']);
    }
	
	public function getUserItem()
    {
        return $this->hasMany(User::className(), ['id' => 'id']);
    }

	public function getProjectEmployee()
    {
        return $this->hasMany(Projects::className(), ['employee' => 'id']);
    }
    
	public function getProjectTeamleder()
    {
        return $this->hasOne(Projects::className(), ['team_leade' => 'id']);
    }
}
