<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "invitations".
 *
 * @property string $item_name
 * @property string $supplier_name
 * @property string $open_date
 * @property string $due_date
 * @property integer $quantity_order
 * @property string $approval_status
 * @property string $order_status
 * @property string $notes
 */
class Invitations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invitations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'supplier_name', 'open_date', 'due_date', 'quantity_order', 'approval_status', 'order_status'], 'required','message'=>'שדה חובה'],
            [['open_date'], 'safe'],
            [['quantity_order'], 'integer','message'=>'ספרות בלבד'],
            [['approval_status', 'order_status'], 'string'],
            [['item_name', 'notes'], 'string', 'max' => 200],
            [['supplier_name'], 'string', 'max' => 100],
            [['due_date'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_name' => 'שם פריט',
            'supplier_name' => 'שם ספק',
            'open_date' => 'תאריך פתיחה',
            'due_date' => 'תאריך אספקה',
            'quantity_order' => 'כמות',
            'approval_status' => 'סטטוס אישור',
            'order_status' => 'סטטוס הזמנה',
            'notes' => 'הערות',
        ];
    }
	
	/*public function getSupplierss()
	{
		return $thus->hasOne(Suppliers::className(),['supplier_name' =>'supplier_name']);
	}
	
	public function getItems()
	{
		return $thus->hasMany(Item::className(),['item_name' =>'item_name']);
	}*/
}
