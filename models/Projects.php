<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $define_project
 * @property string $team_leader
 * @property string $employee
 * @property string $location
 * @property string $due_date
 * @property string $notes
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['define_project', 'team_leader', 'employee', 'location', 'due_date', 'notes'], 'required'],
            [['define_project', 'team_leader',  'location', 'due_date', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'מספר פרויקט',
            'define_project' => 'הגדרת הפרויקט',
            'team_leader' => 'ראש צוות',
            'employee' => 'עובדים',
            'location' => 'מיקום',
            'due_date' => 'תאריכי ביצוע',
            'notes' => 'הערות',
        ];
    }
	
	public function getEmployeesssProject() // שייך לקשר עם טבלת עובדים לשדה של עובדים
    {
        return $this->hasOne(Employees::className(), ['id' => 'employee']);
    }
	
	public function getEmployeessProject() //שייך לקשר עם טבלת עובדים לשדה של ראש צוות
    {
        return $this->hasMany(Employees::className(), ['id' => 'team_leade']);
    }
	
	
	
}
