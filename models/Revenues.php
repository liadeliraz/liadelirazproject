<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "revenues".
 *
 * @property string $date
 * @property string $day
 * @property integer $cash_desk_784
 * @property integer $cash_desk_782
 * @property integer $store
 */
class Revenues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'revenues';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'day', 'cash_desk_784',  'store'], 'required'],
            [['date'], 'safe'],
            [['cash_desk_784', 'cash_desk_782', 'store'], 'integer'],
            [['day'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => 'תאריך',
            'day' => 'יום',
            'cash_desk_784' => 'קופה 784',
            'cash_desk_782' => 'קופה 782',
            'store' => 'חנות',
        ];
    }
}
