<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "suppliers".
 *
 * @property string $supplier_name
 * @property string $phoneNum
 * @property string $address
 * @property string $contact
 * @property string $contactPhone
 * @property string $category
 * @property string $notes
 */
class Suppliers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suppliers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supplier_name', 'phoneNum', 'address','category'], 'required','message'=>'שדה חובה'],
            [['supplier_name'], 'string', 'max' => 100],
            [['phoneNum', 'address', 'contact', 'contactPhone', 'category', 'notes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'supplier_name' => 'שם ספק',
            'phoneNum' => 'טלפון',
            'address' => 'כתובת',
            'contact' => 'איש קשר',
            'contactPhone' => 'טלפון איש קשר',
            'category' => 'קטגוריה',
            'notes' => 'הערות',
        ];
    }
	
	public static function getsupliers()
	{
		$allsupliers = self::find()->all();
		$allsupliersArray = ArrayHelper::
					map($allsupliers, 'supplier_name', 'supplier_name');
		return $allsupliersArray;						
	}
	
	
	
	/*public function getInvitationss()
	{
		return $thus->hasMany(Invitations::className(),['supplier_name' =>'supplier_name']);
	}*/
}
