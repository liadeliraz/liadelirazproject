<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Item;
use app\models\Suppliers;
use dosamigos\datepicker\DateRangePicker;
use kartik\widgets\TouchSpin;
use kartik\widgets\Select2;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Invitations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invitations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'item_name')->dropDownList(Item::getitems(), ['prompt' => 'בחר פריט']) ?>

    <?= $form->field($model, 'supplier_name')->dropDownList(Suppliers::getsupliers(), ['prompt' => 'בחר ספק']) ?>
	
	
	<?=$form->field($model, 'quantity_order')->widget(TouchSpin::classname(), [
				'options'=>['placeholder'=>'הכנס כמות'],
				'pluginOptions' => [
					'verticalbuttons' => true,
					'verticalupclass' => 'glyphicon glyphicon-plus',
					'verticaldownclass' => 'glyphicon glyphicon-minus',
					'min' => 0,
					'max' => 100,
					'boostat' => 5,
				]
				]);?>
	
	
	
	
	
	<?= $form->field($model, 'due_date')->widget(
			DatePicker::className(), [
			// inline too, not bad
			 'inline' => false, 
			 // modify template for custom rendering
			//'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
			'clientOptions' => [
			'autoclose' => true,
			'format' => 'dd/mm/yyyy'
						]
	]); ?>
	
	
	
	
	
	

    

    <?= $form->field($model, 'approval_status')->dropDownList([ 'approved' => 'מאושר', 'not approved' => 'לא מאושר', ], ['prompt' => 'בחר סטטוס']) ?>

    <?= $form->field($model, 'order_status')->dropDownList([ 'provided' => 'סופק', 'not provided' => 'לא סופק', ], ['prompt' => 'בחר סטטוס']) ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
