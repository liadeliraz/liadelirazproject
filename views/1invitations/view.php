<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Invitations */

$this->title = $model->item_name;
$this->params['breadcrumbs'][] = ['label' => 'הזמנות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invitations-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'item_name' => $model->item_name, 'supplier_name' => $model->supplier_name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'item_name' => $model->item_name, 'supplier_name' => $model->supplier_name], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'item_name',
            'supplier_name',
            'open_date',
            'due_date',
            'approval_status',
            'order_status',
            'notes',
        ],
    ]) ?>

</div>
