<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\models\Bstatus;


/* @var $this yii\web\View */
/* @var $model app\models\Buroc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buroc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'treatment')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'bstatus')->dropDownList(Bstatus::getBstatuss()) ?>

	<?= $form->field($model, 'DueDate')->widget(\yii\jui\DatePicker::classname(),
			//DatePicker::className(),
			[
			'dateFormat' => 'dd/MM/yyyy',
			// inline too, not bad
			 //'inline' => false, 
			 // modify template for custom rendering
			//'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
			//'clientOptions' => [
			//'autoclose' => true,
			//'format' => 'yyyy-m-dd'
						
	]); ?>


    <?= $form->field($model, 'notes')->textArea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור  נושא' : 'עדכון', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
