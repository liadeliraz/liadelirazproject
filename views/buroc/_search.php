<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BurocSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buroc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'subject') ?>

    <?= $form->field($model, 'treatment') ?>

    <?= $form->field($model, 'bstatus') ?>

    <?= $form->field($model, 'DueDate') ?>

    <?= $form->field($model, 'creatDate') ?>

    <?php // echo $form->field($model, 'notes') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
