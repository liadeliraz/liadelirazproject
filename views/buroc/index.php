<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BurocSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'בירוקרטיה';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buroc-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור נושא', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('המרה לאקסל', ['export'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'subject',
            'treatment',
            //'bstatus',
           [
				'attribute' => 'bstatus',
				'label' => 'סטטוס',
				'format' => 'raw',
				'value' => function($model){
					return $model->bstatusItem->name;
				},
				'filter'=>Html::dropDownList('BurocSearch[bstatus]', $bstatus, $bstatuss, ['class'=>'form-control']),
			],			
			
			
			
			
			[
				'attribute' => 'DueDate',
				'value' => 'DueDate',
				'format' => 'raw',
				'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'DueDate',
						'clientOptions' => [
						'autoclose' => true,
						'format' => 'yyyy-m-dd']
							
						])
			],
			
			

			//'DueDate',
            'creatDate',
            // 'notes',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
