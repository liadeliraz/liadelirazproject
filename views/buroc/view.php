<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Buroc */

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'בירוקרטיה', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buroc-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עדכון', ['update', 'id' => $model->subject], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->subject], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'subject',
            'treatment',
            //'bstatus',
			[ // the armed name 
				'label' => $model->attributeLabels()['bstatus'],
				'value' => $model->bstatusItem->name,	
			],			
            'DueDate',
            'creatDate',
            'notes',
        ],
    ]) ?>

</div>
