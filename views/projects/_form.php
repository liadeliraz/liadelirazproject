<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\ArrayHelper;
use app\models\Employees;
/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>

    
	
	<?= $form->field($model, 'define_project')->textarea(['maxlength' => true]); ?>


    <?= $form->field($model, 'team_leader')->listBox((Employees::getEmployees()) ); ?>

	
 <?=  $form->field($model, 'employee')->dropDownList(Employees::getEmployees(),['multiple'=>'multiple'])->label("בחר עובדים");?>
	

    

    <?= $form->field($model, 'due_date')->widget(DateRangePicker::className(), [
    'attributeTo' => 'due_date', 
    'form' => $form, // best for correct client validation
    'language' => 'es',
    'size' => '25',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'dd-M-yyyy'
    ]
])->hint('הכנס תאריך התחלה ותאריך סיום');?>
	
	<?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>
	
	

	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'שלח' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
