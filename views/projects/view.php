<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Projects;
use app\models\Employees;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'define_project',
            [ // the team_leader name of the project
				'label' => $model->attributeLabels()['team_leader'],
				'format' => 'html',
				'value' =>$model->employeesssProject->fullname, 
			],
            //[ // the employee name of the project
				//'label' => $model->attributeLabels()['employee'],
				//'format' => 'html',
				//'value' =>$model->employeessProject->fullname, 
			//],
            'location',
            'due_date',
            'notes',
        ],
    ]) ?>

</div>
