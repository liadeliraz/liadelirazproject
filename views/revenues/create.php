<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Revenues */

$this->title = 'יצירת הכנסה';
$this->params['breadcrumbs'][] = ['label' => 'הכנסות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="revenues-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
