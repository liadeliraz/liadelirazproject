<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Revenues */

$this->title = 'Update Revenues: ' . $model->date;
$this->params['breadcrumbs'][] = ['label' => 'Revenues', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->date, 'url' => ['view', 'id' => $model->date]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="revenues-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
