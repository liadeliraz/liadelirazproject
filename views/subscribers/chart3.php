<?php

use miloschuman\highcharts\Highcharts;
?>




<div style="display: none">
	<?php
		echo Highcharts::widget([
		'scripts' => [
			'highcharts-more',
			//'themes/grid',
			'highcharts-3d',
			'modules/drilldown'
		]
			
	]);
	?>	
</div>


<div id="chart3"></div>

<?php


$sql = "select year(g.date) as ghh
,SUM(DISTINCT g.`cash_desk_784`)+ SUM(DISTINCT g.`cash_desk_782`)+ SUM(DISTINCT g.`store`) as rev1
,SUM(DISTINCT g1.`cash_desk_784`)+ SUM(DISTINCT g1.`cash_desk_782`)+ SUM(DISTINCT g1.`store`) as ya
, SUM(DISTINCT g2.`cash_desk_784`)+ SUM(DISTINCT g2.`cash_desk_782`)+ SUM(DISTINCT g2.`store`)  as fv
,SUM(DISTINCT g3.`cash_desk_784`)+ SUM(DISTINCT g3.`cash_desk_782`)+ SUM(DISTINCT g3.`store`) as mr
,SUM(DISTINCT g4.`cash_desk_784`)+ SUM(DISTINCT g4.`cash_desk_782`)+ SUM(DISTINCT g4.`store`) as ap
,SUM(DISTINCT g5.`cash_desk_784`)+ SUM(DISTINCT g5.`cash_desk_782`)+ SUM(DISTINCT g5.`store`) as ma
,SUM(DISTINCT g6.`cash_desk_784`)+ SUM(DISTINCT g6.`cash_desk_782`)+ SUM(DISTINCT g6.`store`) as yo
,SUM(DISTINCT g7.`cash_desk_784`)+ SUM(DISTINCT g7.`cash_desk_782`)+ SUM(DISTINCT g7.`store`) as yol
,SUM(DISTINCT g8.`cash_desk_784`)+ SUM(DISTINCT g8.`cash_desk_782`)+ SUM(DISTINCT g8.`store`) as ag
,SUM(DISTINCT g9.`cash_desk_784`)+ SUM(DISTINCT g9.`cash_desk_782`)+ SUM(DISTINCT g9.`store`) as sp
,SUM(DISTINCT g10.`cash_desk_784`)+ SUM(DISTINCT g10.`cash_desk_782`)+ SUM(DISTINCT g10.`store`) oc
,SUM(DISTINCT g11.`cash_desk_784`)+ SUM(DISTINCT g11.`cash_desk_782`)+ SUM(DISTINCT g11.`store`) as nv
,SUM(DISTINCT g12.`cash_desk_784`)+ SUM(DISTINCT g12.`cash_desk_782`)+ SUM(DISTINCT g12.`store`) as de

from subscribers g
left join subscribers g1 on g.date = g1.date and month(g.date) = 1
left join subscribers g2 on g.date = g2.date and month(g.date) = 2
left join subscribers g3 on g.date = g3.date and month(g.date) = 3
left join subscribers g4 on g.date = g4.date and month(g.date) = 4
left join subscribers g5 on g.date = g5.date and month(g.date) = 5
left join subscribers g6 on g.date = g6.date and month(g.date) = 6
left join subscribers g7 on g.date = g7.date and month(g.date) = 7
left join subscribers g8 on g.date = g8.date and month(g.date) = 8
left join subscribers g9 on g.date = g9.date and month(g.date) = 9
left join subscribers g10 on g.date = g10.date and month(g.date) = 10
left join subscribers g11 on g.date = g11.date and month(g.date) = 11
left join subscribers g12 on g.date = g12.date and month(g.date) = 12

group by year(g.date)";
$rawData = yii::$app->db->createCommand($sql)->queryAll();
$main_data =[];
foreach ($rawData as $data){
	$main_data[] =[
	'name'=>$data['ghh'],
	'data' => [[$data['ya']*1],[$data['fv']*1],[$data['mr']*1],[$data['ap']*1],[$data['ma']*1],[$data['yo']*1],[$data['yol']*1],[$data['ag']*1],[$data['sp']*1],[$data['oc']*1],[$data['nv']*1],[$data['de']*1]]	
	];	
}


$main = json_encode($main_data);


?>

<?php	
	
	$this->registerJs("$(function () {
    $('#chart3').highcharts({
        title: {
            text: 'מגמת מנויים',
            x: -20 //center
        },
        subtitle: {
            text: ' לאורך השנים 2012-2016',
            x: -20
        },
        xAxis: {
            categories: ['ינואר', 'פברואר', 'מרס', 'אפריל', 'מאי', 'יוני',
                'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר']
        },
        yAxis: {
            title: {
                text: 'מנויים'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'מנויים'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: $main
    });
});");
?>




