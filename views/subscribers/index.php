<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubscribersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'מנויים';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('צור מנויים', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('המרה לאקסל', ['export'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('לגרף מנויים', ['chart3'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'date',
            'day',
            'cash_desk_784',
            'cash_desk_782',
            'store',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
