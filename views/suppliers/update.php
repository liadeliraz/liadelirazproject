<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Suppliers */

$this->title = 'עידכון ספק: ' . $model->supplier_name;
$this->params['breadcrumbs'][] = ['label' => 'ספקים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->supplier_name, 'url' => ['view', 'id' => $model->supplier_name]];
$this->params['breadcrumbs'][] = 'עידכון';
?>
<div class="suppliers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
