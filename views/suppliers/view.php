<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Suppliers */

$this->title = $model->supplier_name;
$this->params['breadcrumbs'][] = ['label' => 'ספקים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suppliers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עידכון', ['update', 'id' => $model->supplier_name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->supplier_name], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'האם אתה בטוח שאתה רוצה למחוק פריט זה?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'supplier_name',
            'phoneNum',
            'address',
            'contact',
            'contactPhone',
            'category',
            'notes',
        ],
    ]) ?>

</div>
