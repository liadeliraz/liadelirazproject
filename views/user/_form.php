<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Employees;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php if (\Yii::$app->user->can('createUser')) { ?>
		<?=$form->field($model, 'id')->dropDownList(Employees::getEmployees(),['prompt' => 'בחר עובד']) ?> 
	<?php } ?>
	
	<?php if (\Yii::$app->user->can('createUser')) { ?>
		<?= $form->field($model, 'role')->dropDownList($roles) ?>		
	<?php } ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	<?php if ($model->isNewRecord): ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
	<?php endif; ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
